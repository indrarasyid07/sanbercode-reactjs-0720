//Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var arrayDaftarPesertaObj = {
    nama : "Asep",
    "jenis kelamin" : "Laki-laki",
    hobi : "baca buku",
    "tahun lahir" : 1992
}
console.log(arrayDaftarPesertaObj)

//Soal 2
var buah = [
    {
        nama : "strawberry", 
        warna : "merah", 
        "ada bijinya" : "tidak", 
        harga : 9000,
    }, 
    {
        nama : "jeruk", 
        warna : "oranye", 
        "ada bijinya" : "ada", 
        harga : 8000
    }, 
    {
        nama : "Semangka", 
        warna : "Hijau & Merah", 
        "ada bijinya" : "ada", 
        harga : 10000
    }, 
    {
        nama : "Pisang", 
        warna : "Kuning", 
        "ada bijinya" : "tidak", 
        harga : 5000}]
console.log(buah[0])

//Soal 3
var dataFilm = []
var Film = {
    nama : "Kung Fu Panda",
    durasi : "2 Jam",
    genre : "Action Comedy",
    tahun : 2008
}
function tambahDataFilm(Film){
    dataFilm.push(Film)
}
tambahDataFilm(Film)
console.log(dataFilm[0])

//Soal 4
//Release 0
class Animal {
    constructor(name){
        this.name = name
    }
    get legs(){
        return 4
    }
    get cold_blooded(){
        return "false"
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
//Release 1
class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        console.log("hop hop")
    }
}
class Ape extends Animal{
    constructor(name) {
        super(name)
    }
    get legs(){
        return 2
    }
    yell(){
        console.log("Auooo")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()
 
var kodok = new Frog("buduk")
kodok.jump()

//Soal 5
class Clock{
  constructor({ template }){
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
    }
}
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
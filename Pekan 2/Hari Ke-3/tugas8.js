//Soal 1
console.log("Soal 1")
var rumusLingkaran = (r) => {
    let Luas = r*r*3.14
    let Keliling = 2*r*3.14
    return "Luas Lingkaran " + Luas + "\n" + "Keliling Lingkaran "+ Keliling 
    }
let jariJari = 7
console.log(rumusLingkaran(jariJari))

//Soal 2
console.log("\n Soal 2")
let kata1 = 'saya'
let kata2 = 'adalah'
let kata3 = 'seorang'
let kata4 = 'frontend'
let kata5 = 'developer'
var kalimat = () => {
    let a = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
    console.log(a)
}
kalimat()

//Soal 3
console.log("\n Soal 3")
class Book {
    constructor(name,totalPage,price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}
class Komik extends Book {
    constructor(name,totalPage,price,isColorful) {
        super(name,totalPage,price)
        this.isColorful = isColorful
    }
}

A = new Book("Iron Man",200,20000)
console.log(A)
B = new Komik("Kung Fu Panda",240,30000,false)
console.log(B)
//Soal 1
console.log("\nJawaban Soal 1")
var a = 1;
var b = 0;
console.log("Looping Pertama");
while(a<=10) {
    b+=2;
    console.log(b+" - I love coding");
    a+=1;
}
console.log("Looping Kedua");
while(a>1) {
    console.log(b+" - I will become a frontend developer");
    b-=2;
    a-=1;
}

//Soal 2
console.log("\nJawaban Soal 2")
for(var a=1; a<=20; a++){
    if((a%2)==0){
        console.log(a+" - Berkualitas");
    }
    else if((a%3)==0 && (a%2)==1){
        console.log(a+" - I Love Coding");
    }
    else {
        console.log(a+" - Santai");
    }
}

//Soal 3
console.log("\nJawaban Soal 3")
var c= "#";
for(var a=1; a<=7; a++){
    console.log(c)
    c=c+"#";
}

//Soal 4
console.log("\nJawaban Soal 4")
var kalimat = "saya sangat senang belajar javascript"
var kata = kalimat.split(" ")
console.log(kata)

//Soal 5
console.log("\nJawaban Soal 5")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for(var a=0; a<daftarBuah.length; a++){
    console.log(daftarBuah[a])
}